#!/usr/bin/env python
from os.path import join, exists
from glob import glob
import shutil
import sys
import os
import re

game = 'retro-game'
game_files = join('release', 'game', game)
archive = join('release', game)


def main():

    # clean release folder
    if exists('release'):
        shutil.rmtree('release')
    os.makedirs('release')

    def ignore_files(src, names) -> set:
        '''
        By default, shutil.copytree copies everything.\n
        Here we ignore a bunch of files that should not be shared in a release.
        '''
        bad_ext = re.compile(r'\.(blend\d+|pyc)$')
        bad: set = {'.python', '__pycache__'}
        return set(filter(
            lambda name: (name in bad) or bad_ext.search(name),
            names))

    # move game files to release folder
    shutil.copytree('game', game_files, ignore=ignore_files)

    # try to move the ready-to-install game python package
    try:
        data = sorted(glob(join('dist', 'retro-game-*.tar.gz')))[-1]
        shutil.copyfile(data, join(game_files, 'data.tar.gz'))
    except IndexError:
        return print('retro package is not built', file=sys.stderr)

    # zip everything, should be ready to share
    shutil.make_archive(archive, 'zip', join('release', 'game'))


if __name__ == '__main__':
    main()
