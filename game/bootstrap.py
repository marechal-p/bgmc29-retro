'''
This script is meant to bootstrap the whole game installation,
from within the BGE.
'''
import bge

from typing import NamedTuple, Optional, Callable, Iterable, Mapping, Any
from concurrent.futures import Future, ThreadPoolExecutor
from collections import OrderedDict, deque
from platform import system
from pathlib import Path

import subprocess
import weakref
import sys
import os
import re


is_windows = system() == 'Windows'
is_develop = 'VIRTUAL_ENV' in os.environ
python_bin_linux = Path('bin') / 'python'
python_bin_win = Path('Scripts') / 'python.exe'
python_bin = python_bin_win if is_windows else python_bin_linux


def command(args):
    return [*map(str, args)]


class Canary(object):
    ...


class Job(NamedTuple):
    name: str
    function: Callable[..., Any]
    arguments: Iterable = ()
    keyworded_arguments: Mapping[str, Any] = {}


class RunningJob(NamedTuple):
    name: str
    future: Future


class Bootstrap(bge.types.KX_PythonComponent):

    PROCESS_TIMEOUT = 'Process Timeout'
    VIRTUALENV = 'Virtualenv Path'
    GAME = 'Game'

    args = OrderedDict([
        (GAME, '//blends/game.blend'),
        (VIRTUALENV, '//python'),
        (PROCESS_TIMEOUT, 120),
    ])

    def start(self, args):
        '''
        Setup the bootstrapping:

        - Get Component arguments:
          - Virtualenv Path (to host dependencies)
          - Process Timeout
        - Canary to detect game exit and dispose of resources.
        - Various executable paths.
        - Setup the tasks to be executed:
          - Create a virtualenv.
          - Install dependencies.
          - Run the game using virtualenv.
        '''

        # task system
        self.current_task: Optional[RunningJob] = None
        self.label = self.object.children['text']
        self.todo: Iterable[Job] = deque()
        self.handles = deque()

        # options
        if is_develop:
            self.virtualenv = Path(os.environ['VIRTUAL_ENV'])
        else:
            self.virtualenv = Path(bge.logic.expandPath(args[self.VIRTUALENV]))

        self.process_timeout = args[self.PROCESS_TIMEOUT]
        self.game = bge.logic.expandPath(args[self.GAME])

        # exit detection
        canary = Canary()
        self.object.scene[Canary] = canary
        self.on_exit = weakref.finalize(canary, self.exit)

        # paths
        self.python_virtualenv = self.virtualenv / python_bin
        self.python_blender = None
        self.blender = Path(sys.executable)
        self.player = self.blender.parent / 'blenderplayer'
        self.root = Path(__file__).parent

        # python executable lookup
        python_name = re.compile(r'python(\d(\.\d)?\w?)?(\.exe)?')
        for path in (Path(sys.prefix) / 'bin').glob('python*'):
            if python_name.search(str(path)):
                self.python_blender = path
                break

        if self.python_blender is None:
            raise FileNotFoundError('Could not find a valid Python interpreter.')

        # async worker
        self.thread_worker = ThreadPoolExecutor(max_workers=1)

        # task setup
        if not (is_develop or self.python_virtualenv.exists()):
            self.todo.append(Job('Create Virtualenv', self.create_virtualenv))
            self.todo.append(Job('Upgrade Pip', self.upgrade_pip))
            self.todo.append(Job('Install Dependencies', self.install_dependencies))
        self.todo.append(Job('Run Game', self.run_game))

        self.setup_handles()

    def setup_handles(self):
        for i in range(len(self.todo)):
            handle = self.object.scene.addObject('suzanne')
            handle.worldPosition = i - 2.5, -1.5, -3.5
            handle.worldScale *= .5
            self.handles.append(handle)

    def set_label(self, message: str):
        self.label['Text'] = message

    def update(self):
        '''
        Processes the bootstrapping tasks.
        Called each frame.
        '''

        # task execution
        if (self.current_task is None) and (len(self.todo) > 0):
            job = self.todo.popleft()
            self.set_label(job.name)
            print('Running: "%s"' % job.name)
            future = self.thread_worker.submit(job.function, *job.arguments, **job.keyworded_arguments)
            self.current_task = RunningJob(job.name, future)
            future.add_done_callback(self.finish_task)

        # handle animation
        if (len(self.handles) > 0):
            self.handles[-1].applyRotation((0, 0, .1), False)

    def exit(self):
        '''
        Callback called when we detect game exit (bootstrapper only).
        '''

        # shutdown the thread worker
        print('Waiting for the thread worker to shutdown...')
        self.thread_worker.shutdown(wait=True)
        print('Done.')

    def exception(self, exc: Exception):
        '''
        Callback handling exceptions while bootstrapping.
        Exists the game.
        '''

        # print the error and exit the game
        print('Error:', exc, file=sys.stderr)
        bge.logic.endGame()

    def finish_task(self, future: Future):
        '''
        Callback hooked on job's finished event.
        '''
        task_name = '<None>'

        # try to grab the job's result
        try:
            task_name = self.current_task.name  # type: ignore
            result = future.result()

        # job raised an exception
        except Exception as exc:
            print('Task errored: %s = %s' % (task_name, exc))
            self.exception(exc)
            return

        # job completed
        print('Task finished: "%s" = %s' % (task_name, result))
        self.current_task = None

        # remove handle
        handle = self.handles.pop()
        if not handle.invalid:
            handle.endObject()

    def create_virtualenv(self):
        '''
        Setup the virtualenv to host the Python dependencies.
        '''

        # call the `venv` module to create a virtualenv from current python
        return self._exec(
            [self.python_blender, '-m', 'venv', self.virtualenv],
            'Could not create the virtualenv. (%(code)s)')

    def upgrade_pip(self):
        '''
        Grab the latest version of pip.
        '''

        # call the `venv` module to create a virtualenv from current python
        return self._exec(
            [self.python_virtualenv, '-m', 'pip', 'install', '--upgrade', 'pip'],
            'Could not update pip. (%(code)s)')

    def install_dependencies(self):
        '''
        Actually install and upgrade the Python dependencies.
        '''

        return self._exec(
            [self.python_virtualenv, '-m', 'pip', 'install', self.root / 'data.tar.gz'],
            'Could not install dependencies. (%(code)s)')

    def run_game(self):
        '''
        Run the game and use the Python virtualenv.
        '''

        # fetch PYTHONPATH from the virtualenv
        pythonpath = subprocess.check_output(command([
            self.python_virtualenv, '-c', 'import sys, os; print(os.pathsep.join(sys.path), end="")'
        ])).decode()

        # create new environment
        env = os.environ.copy()
        env['PYTHONPATH'] = pythonpath

        # actually start the game using new env
        subprocess.Popen(
            command([self.player, self.game]),
            env=env, cwd=str(self.root))

        bge.logic.endGame()
        return 'Launched'

    def _exec(self, args: list, error_msg: str) -> int:
        code = subprocess.call(command(args), timeout=self.process_timeout)
        if code != 0:
            raise RuntimeError(error_msg % {'code': code})
        return 0
