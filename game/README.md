# Retro Game How-To

## Run the Game

Run `main.blend` using `UPBGE 0.2.4`.

I do `<upbgeplayer> main.blend`, but you can also open the file in the editor and press `P` if you don't know better.

## Play the Game

Right now I just output stuff in the terminal, I hope you are having as much fun as I do.

More to come, maybe.
