from bge import render

from retro.core.event import Event

ON_PAUSE = Event()
PAUSED = False


def pause():
    global PAUSED
    PAUSED = True
    render.showMouse(True)
    ON_PAUSE.fire(True)


def resume():
    global PAUSED
    PAUSED = False
    render.showMouse(False)
    ON_PAUSE.fire(False)
