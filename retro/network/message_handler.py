import bge

from websockets import client, exceptions
from typing import ClassVar

from retro.singletons.message_manager import MessageManager

from retro.network.protocol import (
    MessageType,
    parse_message,
    parse_identification,
    parse_entity_create,
    parse_entity_update,
    parse_entity_delete,
    parse_entity_attack,
)

from retro.scripts.entity_manager import EntityManager
from retro.core.asynchronous import asynchronous, gather

__all__ = [
    'MessageHandler',
]


class Registry(dict):

    def register(self, id, element=None):
        def decorator(e):
            self[id] = e

        if element is None:
            return decorator

        decorator(element)


class MessageHandler:

    HANDLERS: ClassVar = Registry()

    def __init__(self, websocket: client.Connect, entity_manager: EntityManager):
        self.entity_manager = entity_manager
        self.websocket = websocket
        self.mainloop()

    @asynchronous
    async def mainloop(self):
        ws = self.websocket
        while not ws.closed:

            try:
                message = await ws.recv()
            except exceptions.ConnectionClosed:
                print('connection closed.')
                bge.logic.endGame()
                break

            type, raw = parse_message(message)
            handler = self.HANDLERS.get(type, None)
            if handler is None:
                print(f'Unknown message type: {type}')
                continue

            handler(self, raw)

            try:
                await gather(*(ws.send(message)
                    for message in MessageManager.iter_pop_left()))
            except exceptions.ConnectionClosed:
                print('connection closed.')
                bge.logic.endGame()
                break

    @HANDLERS.register(MessageType.Identification)
    def identification(self, raw):
        id, spawn = parse_identification(raw)
        self.entity_manager.initialize(id, spawn)

    @HANDLERS.register(MessageType.EntityCreate)
    def entity_create(self, raw):
        id, type = parse_entity_create(raw)
        self.entity_manager.create(id, type)

    @HANDLERS.register(MessageType.EntityUpdate)
    def entity_update(self, raw):
        entities = parse_entity_update(raw)
        self.entity_manager.update(entities)

    @HANDLERS.register(MessageType.EntityAttack)
    def entity_attack(self, raw):
        attacker, hit, weapon, origin, impact = parse_entity_attack(raw)
        self.entity_manager.attack(attacker, hit, weapon, origin, impact)

    @HANDLERS.register(MessageType.EntityDelete)
    def entity_delete(self, raw):
        id = parse_entity_delete(raw)
        self.entity_manager.delete(id)
