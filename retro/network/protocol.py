# flake8: noqa: E302
from typing import Tuple, List
from struct import Struct

from retro.utility import Vector3


class MessageType:
    PlayerUpdate = 0
    PlayerAttack = 1
    PlayerDeath = 2
    EntityDelete = 3
    EntityUpdate = 4
    EntityAttack = 5
    EntityDeath = 6
    EntityCreate = 7
    Identification = 8


def split(buffer, index):
    return buffer[:index], buffer[index:]


MessageTypeStruct = Struct('<B')
def parse_message(raw: bytes):
    raw_type, raw_data = split(raw, MessageTypeStruct.size)
    return MessageTypeStruct.unpack(raw_type)[0], raw_data


IdentificationStruct = Struct('<I 3f')
def parse_identification(raw: bytes) -> Tuple[int, Vector3]:
    id, *position = IdentificationStruct.unpack(raw)
    return id, position  # type: ignore


EntityCreateStruct = Struct('<IB')
def parse_entity_create(raw: bytes) -> Tuple[int, int]:
    return EntityCreateStruct.unpack(raw)  # type: ignore


EntityUpdateHeadStruct = Struct('<I')
EntityUpdateStruct = Struct('<I 9f')
def parse_entity_update(raw: bytes) -> List[Tuple[int, Vector3, Vector3, Vector3]]:
    raw_length, raw_data = split(raw, EntityUpdateHeadStruct.size)
    length = EntityUpdateHeadStruct.unpack(raw_length)[0]
    entities = [(id, data[:3], data[3:6], data[6:9])
        for id, *data in EntityUpdateStruct.iter_unpack(raw_data)]
    assert len(entities) == length
    return entities  # type: ignore


EntityAttackStruct = Struct('<3I 6f')
def parse_entity_attack(raw: bytes) -> Tuple[int, int, int, Vector3, Vector3]:
    attacker, hit, weapon, *data = EntityAttackStruct.unpack(raw)
    if hit == 0x_FF_FF_FF_FF: hit = None
    return [  # type: ignore
        attacker, hit, weapon,
        data[0:3], data[3:6],
    ]


EntityDeleteStruct = Struct('<I')
def parse_entity_delete(raw: bytes) -> int:
    return EntityDeleteStruct.unpack(raw)[0]


PlayerUpdateStruct = Struct('<9f')
def write_player_update(position: Vector3, rotation: Vector3, velocity: Vector3) -> bytes:
    header = MessageTypeStruct.pack(MessageType.PlayerUpdate)
    return header + PlayerUpdateStruct.pack(*position, *rotation, *velocity)

PlayerAttackStruct = Struct('<I I 3f 3f')
def write_player_attack(hit: int, weapon: int, origin: Vector3, impact: Vector3):
    header = MessageTypeStruct.pack(MessageType.PlayerAttack)
    return header + PlayerAttackStruct.pack(hit, weapon, *origin, *impact)
