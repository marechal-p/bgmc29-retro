import bge

from typing import Any
from collections import OrderedDict

from retro.core.library import setdefault_library
from retro.core.asynchronous import event_loop, sleep
from retro.core.logic import scripts

from retro.utility import print_error


class MainLoop(bge.types.KX_PythonComponent):

    __initialized__ = False
    args: Any = OrderedDict()

    def start(self, args):
        cls = self.__class__
        if cls.__initialized__:
            self.update = lambda: None
        else:
            cls.__initialized__ = True
            self.update = self._update
            self.initialize()

            try:
                from retro.__game__ import main
            except Exception as exc:
                print_error(exc)
                bge.logic.endGame()
                return

            # remove binding on `Esc` to quit the game
            bge.logic.setExitKey(0xFF)

            # execute main entry point
            main(self)

    def initialize(self):
        setdefault_library(self.object.scene, '__main__')

    def _update(self):

        for script in scripts.copy():
            try:
                script()
            except Exception as exc:
                print_error(exc)
                bge.logic.endGame()

        frametime = 1 / bge.logic.getLogicTicRate()
        event_loop.run_until_complete(sleep(frametime / 2))
