import os

from pathlib import Path

from retro.core.configuration import Configuration

root = Path(os.getcwd())

sounds_path = root / 'sounds'
configurations_path = root / 'configurations'
configurations_path.mkdir(parents=True, exist_ok=True)

keybindings = Configuration(configurations_path / 'keybindings.toml')
keybindings.load()
