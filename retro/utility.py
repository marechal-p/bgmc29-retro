import bge

import traceback
import time
import sys

from typing import Union, Set, List, Tuple, Any
from operator import sub

from retro.core.asynchronous import asynchronous
from retro.core.logic import next_frame

Vector3 = Tuple[float, float, float]


def print_error(exc):
    traceback.print_exception(type(exc), exc, exc.__traceback__, file=sys.stderr)


def try_remove(container: Union[Set, List], item: Any) -> None:
    try:
        container.remove(item)
    except ValueError:
        ...
    except KeyError:
        ...


def iter_parents(object):
    current = object
    while current is not None:
        yield current
        current = current.parent


@asynchronous
async def animate_mist(
    world, *,
    start=None,
    distance=None,
    color=None,
    zenith=None,
    horizon=None,
    duration=0,
):
    if start is None:
        start = world.mistStart
    if distance is None:
        distance = world.mistDistance
    if color is None:
        color = world.mistColor
    if zenith is None:
        zenith = world.zenithColor
    if horizon is None:
        horizon = world.horizonColor

    now = time.time()
    deadline = now + duration
    while now < deadline:

        d_time = deadline - now
        d_frames = int(d_time * bge.logic.getLogicTicRate())

        if d_frames <= 0:
            await next_frame()
            break

        d_start = start - world.mistStart
        d_distance = distance - world.mistDistance
        d_color = tuple(map(sub, color, world.mistColor))
        d_zenith = tuple(map(sub, zenith, world.zenithColor))
        d_horizon = tuple(map(sub, horizon, world.horizonColor))

        f = 1 / d_frames
        world.mistStart += d_start * f
        world.mistDistance += d_distance * f
        world.mistColor = tuple(map(lambda x, y: x + y * f, world.mistColor, d_color))
        world.zenithColor = tuple(map(lambda x, y: x + y * f, world.zenithColor, d_zenith))
        world.horizonColor = tuple(map(lambda x, y: x + y * f, world.horizonColor, d_horizon))

        await next_frame()
        now = time.time()

    world.mistStart = start
    world.mistDistance = distance
    world.mistColor = color
    world.zenithColor = zenith
    world.horizonColor = horizon
