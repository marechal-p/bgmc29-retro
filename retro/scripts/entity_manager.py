import bge

from typing import ClassVar, Optional, Dict
from weakref import WeakValueDictionary

from retro.utility import Vector3
from retro.core.entity import EntityFactory
from retro.scripts.player import Player


class EntityManager:

    TYPES: ClassVar[Dict[int, EntityFactory]] = {}

    def __init__(self):
        self.entities = WeakValueDictionary()
        self.player_id: Optional[int] = None
        self.player: Optional[Player] = None

    def initialize(self, id: int, position: Vector3) -> None:
        if self.player_id is not None:
            return print('received two identifications!')
        if self.player is None:
            print('player should have been set by now!')
        else:
            self.player.set_position(position)
        self.player_id = id

    def create(self, id: int, type: int) -> None:
        if id == self.player_id:
            return  # skip self

        if id in self.entities:
            return print(f'entity id already exists: {id}')

        if type not in self.TYPES:
            return print(f'unknown entity type: {type}')

        scene = bge.logic.getCurrentScene()
        self.entities[id] = self.TYPES[type].spawn(id, scene)

    def update(self, data) -> None:
        for entity_data in data:
            id, position, rotation, velocity = entity_data
            if id == self.player_id:
                continue  # skip self

            entity = self.entities.get(id, None)
            if entity is None:
                print(f'unknown entity id: {id}')
                continue

            self.entities[id].update(position, rotation, velocity)

    def attack(self, attacker, hit, weapon, origin, impact):
        if attacker == self.player_id:
            return

        bge.render.drawLine(origin, impact, (1, 0, 0))

    def delete(self, id: int) -> None:
        entity = self.entities.pop(id, None)
        if entity is not None:
            entity.delete()
