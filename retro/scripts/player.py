from typing import NamedTuple, Optional, Tuple, Iterable, Any

from mathutils import Vector

import math

from retro.singletons.message_manager import MessageManager
from retro.network.protocol import write_player_update

from retro.core.item import Item
from retro.input.types import Axis, Button
from retro.input.keymap import Keymap
from retro.input.bge import Mouse, bge_inputs

from retro.utility import Vector3
from retro import game_data, game_state

from retro.weapons.weapon_test import WeaponTest

__all__ = [
    'Player'
]

PlayerKeymap = Keymap(bge_inputs)


def button_to_axis_positive(button: Button):
    return 1. if button.active() else 0.


def button_to_axis_negative(button: Button):
    return -1. if button.active() else 0.


class Collision(NamedTuple):
    object: Any
    point: Vector3
    normal: Vector3
    points: Iterable[Vector3] = ()


class Player:

    # view movement
    look_vertical = PlayerKeymap.define('look-vertical', Axis, ['mouse.dy'])
    look_horizontal = PlayerKeymap.define('look-horizontal', Axis, ['mouse.dx'])

    # character movement using axises
    movement_y = PlayerKeymap.define('movement-y', Axis)
    movement_x = PlayerKeymap.define('movement-x', Axis)

    # character movement using buttons
    forward = PlayerKeymap.define('forward', Button, ['w'])
    backward = PlayerKeymap.define('backward', Button, ['s'])
    left = PlayerKeymap.define('left', Button, ['a'])
    right = PlayerKeymap.define('right', Button, ['d'])

    # link the buttons to the axis for character movement
    PlayerKeymap.link('movement-y', 'forward', button_to_axis_positive)
    PlayerKeymap.link('movement-y', 'backward', button_to_axis_negative)
    PlayerKeymap.link('movement-x', 'right', button_to_axis_positive)
    PlayerKeymap.link('movement-x', 'left', button_to_axis_negative)

    # main actions
    fire = PlayerKeymap.define('fire', Button, ['leftmouse'])

    # sprint/walk modifier
    sprint = PlayerKeymap.define('sprint', Button, ['leftshift'])
    walk = PlayerKeymap.define('walk', Button, ['leftctrl'])

    # actually load the key bindings
    PlayerKeymap.load(game_data.keybindings['player'])

    MOVEMENT_FORCE = 6.
    MOVEMENT_FORCE_AMPLIFICATION = 3
    MOVEMENT_ANGLE = .25 * math.pi
    MOVEMENT_SPRINT_FACTOR = 2
    MOVEMENT_WALK_FACTOR = 0.3

    MOUSE_LOOK_MIN = .05 * math.pi
    MOUSE_LOOK_MAX = .95 * math.pi
    MOUSE_SENSITIVITY = .1

    def __init__(self, player, websocket):
        self.websocket = websocket  # network handle
        self.player = player  # BGE object handle
        self.player.worldPosition.z += 2  # spawn above the ground
        self.camera = player.children['player.camera']  # first person view
        self.player.collisionCallbacks.append(self.__collision)  # grab collisions for last frame
        self.collision: Optional[Collision] = None  # collision that happened on that frame
        self.__weapon = WeaponTest(self)  # TODO: use proper inventory
        self.set_first_person_view()

    def __collision(self, object, point, normal, points):
        self.collision = Collision(object, point, normal, tuple(points))

    def set_first_person_view(self):
        self.player.scene.active_camera = self.camera

    def get_equipped(self) -> Optional[Item]:
        return self.__weapon

    def __call__(self):
        self.fall_back()
        movement = Vector((0, 0, 0))

        if not game_state.PAUSED:
            self._look_around()
            self._use_equipped()
            movement = self._move_around(movement)
            self._send_update()
            Mouse.center()

        self.apply_force(movement)

    def fall_back(self):
        if self.player.worldPosition.z < -30:
            self.player.setLinearVelocity((0, 0, 0))
            self.player.worldPosition = 0, 0, 2

    def _look_around(self):
        look = Vector((
            self.look_vertical.threshold(1e-3, 0),
            0,
            self.look_horizontal.threshold(1e-3, 0),
        )) * self.MOUSE_SENSITIVITY

        head_rotation = self.camera.localOrientation.to_euler()
        head_rotation.x = abs(head_rotation.x + look.x)
        if head_rotation.x > self.MOUSE_LOOK_MAX:
            head_rotation.x = self.MOUSE_LOOK_MAX
        elif head_rotation.x < self.MOUSE_LOOK_MIN:
            head_rotation.x = self.MOUSE_LOOK_MIN

        self.camera.localOrientation = head_rotation
        self.player.applyRotation((0, 0, look.z), False)

    def _move_around(self, fallback):
        collision = self.collision
        self.collision = None

        if collision is None:
            return fallback

        # TODO: BOGUS line, fix should only allow movement on ground.
        # if any(point.normal.angle((0, 0, -1)) > self.MOVEMENT_ANGLE for point in collision.points):
        #     return fallback

        movement_direction = Vector((
            self.movement_x.threshold(1e-3, 0),
            self.movement_y.threshold(1e-3, 0),
            0,
        ))
        movement_direction.normalize()

        speed_factor = self.MOVEMENT_FORCE
        if self.walk.active():
            speed_factor *= self.MOVEMENT_WALK_FACTOR
        elif self.sprint.active():
            speed_factor *= self.MOVEMENT_SPRINT_FACTOR

        return self.player.getAxisVect(movement_direction * speed_factor)

    def _use_equipped(self):
        equipped = self.get_equipped()
        if equipped and self.fire.active():
            equipped.use()

    def _send_update(self):
        MessageManager.queue(write_player_update(
            tuple(self.player.worldPosition),
            (0, 0, self.player.worldOrientation.to_euler().z),
            tuple(self.player.getLinearVelocity()),
        ))

    def apply_force(self, movement):
        speed = self.player.getLinearVelocity(False)
        target = Vector((movement.x, movement.y, speed.z))
        delta = target - speed
        delta.z = 0

        self.player.applyForce(delta * self.player.mass * self.MOVEMENT_FORCE_AMPLIFICATION, False)

    def set_position(self, position):
        self.player.worldPosition = tuple(position)
