from retro.core.entity import Entity
from retro import resource

__all__ = [
    'Zombie'
]


class Zombie(Entity):

    RESOURCE = resource.player
    OBJECT = 'player'
    TYPE = 2

    def create(self, object):
        object.localScale = .5, .5, 1
        object.color = 1, 0, 0, 1
        object.gravity = 0, 0, 0
        # object.mass = 1e3
