from bge import render, logic

import time

from retro.core.sound import Sound
from retro.input.keymap import Keymap
from retro.input.bge import Button, bge_inputs

from retro.utility import iter_parents
from retro import game_data, game_state

__all__ = [
    'GameScript',
]

GameKeymap = Keymap(bge_inputs)


class GameScript:

    toggle_fullscreen = GameKeymap.define('toggle-fullscreen', Button, ['f11'])

    pause = GameKeymap.define('pause', Button, ['esc'])

    GameKeymap.load(game_data.keybindings['game'])

    def __init__(self):
        self.last_pause = 0

    def __call__(self):
        self.handle_sound_listener()
        self.handle_fullscreen()
        self.handle_game_state()

    def handle_sound_listener(self):
        '''
        Sound system needs to know about the listener's location.
        '''
        camera = logic.getCurrentScene().active_camera
        listener_velocity = 0, 0, 0

        for parent in iter_parents(camera):
            if parent.getPhysicsId():
                listener_velocity = parent.getLinearVelocity(False)
                break

        Sound.Device.listener_location = camera.worldPosition
        Sound.Device.listener_orientation = camera.worldOrientation.to_quaternion()
        Sound.Device.listener_velocity = listener_velocity

    def handle_fullscreen(self):
        '''
        Swap between fullscreen and windowed.
        '''
        if self.toggle_fullscreen.pressing():
            render.setFullScreen(not render.getFullScreen())

    def handle_game_state(self):
        '''
        Handles the game state, such as the pause menu.
        '''
        if self.pause.pressing():

            # if we press pause twice quickly, quit
            now = time.time()
            if now < self.last_pause + .5:
                return logic.endGame()
            self.last_pause = now

            # change game pause state (global)
            if game_state.PAUSED:
                game_state.resume()
            else:
                game_state.pause()
