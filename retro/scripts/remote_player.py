from retro.core.entity import Entity
from retro import resource

__all__ = [
    'RemotePlayer'
]


class RemotePlayer(Entity):

    RESOURCE = resource.player
    OBJECT = 'player'
    TYPE = 1
