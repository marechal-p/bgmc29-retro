from retro.core.sound import Sound
from retro import game_data

FiringSound = Sound(str(
    game_data.sounds_path / '414885__mattix__retro-laser-shot-03.wav'),
    volume=.1, buffered=True)

AttackHitSound = Sound(str(
    game_data.sounds_path / '350924__cabled-mess__hurt-c-02.wav'),
    volume=10, buffered=True)
