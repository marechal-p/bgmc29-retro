from collections import deque

from retro.core.singleton import Singleton


class MessageManager(Singleton):

    messages: deque = deque()

    @classmethod
    def queue(cls, message: bytes) -> None:
        cls.messages.append(message)

    @classmethod
    def iter_pop_left(cls):
        while len(cls.messages) > 0:
            yield cls.messages.popleft()
