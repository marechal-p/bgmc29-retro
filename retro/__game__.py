import bge

import getpass
import sys
import os

from websockets import client, exceptions
from aiohttp import ClientSession
from aiofile import AIOFile

from retro.game_data import configurations_path
from retro.network.message_handler import MessageHandler
from retro.core.asynchronous import asynchronous
from retro.core.entity import EntityFactory
from retro.core.logic import scripts

from retro import resource, utility, game_data

from retro.scripts.game_script import GameScript
from retro.scripts.entity_manager import EntityManager
from retro.scripts.player import Player
from retro.scripts.zombie import Zombie
from retro.scripts.remote_player import RemotePlayer

BGMC_USERNAME = os.environ.get('BGMC_USERNAME', getpass.getuser())
SERVER = os.environ.get('BGMC_SERVER', 'ws://bgmc29-server.herokuapp.com')


@asynchronous
async def register():
    async with ClientSession() as session:
        async with session.get(SERVER + '/player/register', headers={ 'bgmc-username': BGMC_USERNAME }) as response:
            data = await response.json()
            if data.get('success', False):
                return data['uuid']
            else:
                print(data.get('message', 'Error'), file=sys.stderr)
                bge.logic.endGame()
                return None


@asynchronous
async def main(component):
    game_data.keybindings.save()
    scripts.append(GameScript())
    scene = component.object.scene
    world = scene.world

    # setup mist
    world.mistEnable = True
    world.mistType = world.KX_MIST_LINEAR
    world.mistStart = 0
    world.mistDistance = 0
    world.mistIntensity = 0
    world.mistColor = 0, 0, 0
    world.zenithColor = 0, 0, 0, 1
    world.horizonColor = 0, 0, 0, 1

    file_path = configurations_path / 'user.data'

    if file_path.exists():
        async with AIOFile(str(file_path), 'r') as file:
            content = await file.read()
        uuid = content.strip()

    else:
        uuid = await register()
        if uuid is None:
            return bge.endGame()

        async with AIOFile(str(file_path), 'w') as file:
            await file.write(uuid)
            await file.fsync()

    # setup websocket connection
    ENDPOINT = SERVER + '/ws/game/play/0'
    try:
        print(f'Connecting to {ENDPOINT}')
        websocket = await client.Connect(ENDPOINT, extra_headers={ 'bgmc-retro-uuid': uuid })
        print('Connected.')

    except Exception as exc:
        utility.print_error(exc)
        return bge.logic.endGame()

    # load player model and create entity manager
    player_lib = await resource.player.load()
    entity_manager = EntityManager()

    # register the entity types
    for entity_class in [RemotePlayer, Zombie]:
        type, factory = await EntityFactory.New(entity_class)
        entity_manager.TYPES[type] = factory

    # spawn player
    player_object = scene.addObject(player_lib.inactives['player'])
    player = Player(player_object, websocket)
    entity_manager.player = player

    # run systems
    scripts.append(player)
    MessageHandler(websocket, entity_manager)

    utility.animate_mist(world, start=5, distance=25, horizon=(.02, .01, .02, 1), duration=1)
