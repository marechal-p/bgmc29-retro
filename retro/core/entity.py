from typing import ClassVar, Optional, Type

from retro.core.asynchronous import asynchronous
from retro.core.libload import Blend

__all__ = [
    'Entity',
    'EntityFactory',
]


class Entity:

    GAME_PROPERTY = '__entity_id__'

    RESOURCE: ClassVar[Optional[Blend]]
    OBJECT: Optional[str]
    TYPE: ClassVar[int]

    def __init__(self, id: int, object):
        object[self.GAME_PROPERTY] = id
        self.object = object
        self.id = id
        self.create(object)

    def create(self, object):
        ...

    def update(self, position, rotation, velocity):
        self.update_positon(position)
        self.update_rotation(rotation)
        self.update_velocity(velocity)

    def update_positon(self, position):
        self.object.worldPosition = position

    def update_rotation(self, rotation):
        new_rotation = self.object.worldOrientation.to_euler()
        new_rotation.z = rotation[2]
        self.object.worldOrientation = new_rotation

    def update_velocity(self, velocity):
        self.object.setLinearVelocity(velocity, False)

    def delete(self):
        self.object.endObject()


class EntityFactory:

    @asynchronous
    async def New(entity_class):
        library = await entity_class.RESOURCE.load()
        return entity_class.TYPE, EntityFactory(
            entity_class,
            library.inactives[entity_class.OBJECT]
        )

    def __init__(self, entity_class: Type[Entity], template_object):
        self.template_object = template_object
        self.entity_class = entity_class

    def spawn(self, id, scene):
        object = scene.addObject(self.template_object)
        entity = self.entity_class(id, object)
        object['__entity__'] = entity
        return entity
