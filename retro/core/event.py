from retro.utility import try_remove


class Event:

    def __init__(self):
        self.listeners = []

    def add_listener(self, callback):
        self.listeners.append(callback)

    def remove_listener(self, callback):
        try_remove(self.listeners, callback)

    def fire(self, *args, **kargs):
        for listener in self.listeners:
            listener(*args, **kargs)
