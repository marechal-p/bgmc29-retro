from retro.core.asynchronous import Future, create_future

scripts = []


async def next_frame(count=1):
    for _ in range(count):
        await __frame_future__


@scripts.append
def __next_frame__() -> Future:
    global __frame_count__, __frame_future__
    __frame_count__ += 1
    __frame_future__.set_result(__frame_count__)
    __frame_future__ = create_future()
__frame_future__ = create_future()
__frame_count__ = 0
