from asyncio import (  # noqa
    Future, AbstractEventLoop, get_event_loop, sleep,
    gather, wait, wait_for,
)
from asyncio import *  # noqa
import functools
import inspect

event_loop: AbstractEventLoop = get_event_loop()


def create_future():
    '''
    Creates a future bound to the current event loop.
    '''
    return event_loop.create_future()


def asynchronous(function):
    '''
    Register an async function so that it gets run in the event loop.
    '''
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        return event_loop.create_task(maybe_awaitable(function(*args, **kwargs)))
    return wrapper


async def maybe_awaitable(value):
    '''
    If `value` is awaitable, then await.
    If `value` is not awaitable, this method will just pass the value asynchronously.
    '''
    if inspect.isawaitable(value):
        return await sleep(0, await value)
    return await sleep(0, value)
