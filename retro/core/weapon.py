from retro.core.item import Item


class Weapon(Item):

    def use(self):
        self.fire()

    def fire(self):
        ...
