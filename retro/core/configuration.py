import toml

from pathlib import Path

__all__ = [
    'Configuration'
]


class ConfigurationEntry(dict):

    def __init__(self, source={}):
        self.update(source)

    def __missing__(self, name):
        self[name] = object = ConfigurationEntry()
        return object

    def update(self, other):
        for name, item in other.items():
            if isinstance(item, dict) and isinstance(self[name], dict):
                self[name].update(item)
            else:
                self[name] = item


class Configuration(ConfigurationEntry):

    def __init__(self, path: Path, defaults={}):
        self._defaults = defaults.copy()
        self.path = path
        self.update(defaults)

    def save(self):
        with self.path.open('w') as file:
            toml.dump(dict(self), file)  # toml WANTS a dict.

    def load(self):
        try:
            with self.path.open('r') as file:
                data = toml.load(file)
        except FileNotFoundError:
            data = {}

        self.update(data)
