import bge

from weakref import WeakKeyDictionary

from . import scripts
from ..utility import try_remove


class Filter(bge.types.KX_2DFilter):

    index: int

    def __init__(self, object):
        ...


class Shader:

    def __init__(self):
        self.scenes = WeakKeyDictionary()

    def attach(self, scene=None, index: int = 0) -> None:

        if scene is None:
            scene = bge.logic.getCurrentScene()

        if scene in self.scenes:
            return

        scene.preDrawSetup.append(self.pre_draw_setup)
        scene.preDraw.append(self.pre_draw)
        scene.postDraw.append(self.post_draw)
        scripts.append(self.logic)

        filter = Filter(scene.filterManager.addFilter(index))
        filter.index = index
        self.setup(filter)

        self.scenes.add(scene, filter)

    def detach(self, scene=None) -> None:

        if scene is None:
            scene = bge.logic.getCurrentScene()

        try_remove(scene.preDrawSetup, self.pre_draw_setup)
        try_remove(scene.preDraw, self.pre_draw)
        try_remove(scene.postDraw, self.post_draw)
        try_remove(scripts, self.logic)

        if scene in self.scenes:
            self.scenes.pop(scene, None)

    def destroy(self) -> None:
        for scene in self.scenes:
            self.detach(scene)

    def setup(self, filter) -> None:
        ...

    def logic(self) -> None:
        ...

    def pre_draw_setup(self, camera) -> None:
        ...

    def pre_draw(self, camera) -> None:
        ...

    def post_draw(self) -> None:
        ...
