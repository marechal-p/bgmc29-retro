from typing import NamedTuple, MutableMapping, Any
from weakref import WeakValueDictionary

__all__ = [
    'Library',
    'setdefault_library'
]


class Library(NamedTuple):
    name: str
    actives: MutableMapping[str, Any]
    inactives: MutableMapping[str, Any]


def setdefault_library(scene, name: str) -> Library:
    inactives: MutableMapping[str, Any] = WeakValueDictionary()
    actives: MutableMapping[str, Any] = WeakValueDictionary()

    for object in scene.objects:
        if '__library__' not in object:
            object['__library__'] = name
            actives.setdefault(object.name, object)

    for object in scene.objectsInactive:
        if '__library__' not in object:
            object['__library__'] = name
            inactives.setdefault(object.name, object)

    return Library(name, actives, inactives)
