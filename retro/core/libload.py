import bge

from typing import Optional, Callable, List, Any

from retro.core.asynchronous import Future, create_future, asynchronous
from retro.core.library import Library, setdefault_library


class Blend:
    '''
    Better libload?
    '''

    def __init__(self,
        path: str, *,
        data: Optional[Callable] = None, type: str = 'Scene',
        load_actions: bool = True, load_scripts: bool = True,
        asynchronous: bool = True, verbose: bool = False,
    ):
        self.path = path
        self.data = data
        self.type = type
        self.load_actions = load_actions
        self.load_scripts = load_scripts
        self.asynchronous = asynchronous
        self.verbose = verbose

        self.scene: Optional = None
        self.status: Optional = None
        self.future: Optional[Future[Library]] = None

    def load(self, scene: Optional[Any] = None) -> Future:
        if self.future is not None:
            return self.future

        self.future = create_future()

        try:
            if scene is None:
                scene = bge.logic.getCurrentScene()

            args: List[Any] = [self.path, self.type]

            data = self.data
            if callable(data):
                data = data(self)
            if data is not None:
                args.append(data)

            self.status = status = bge.logic.LibLoad(
                *args,
                load_actions=self.load_actions,
                verbose=self.verbose,
                load_scripts=self.load_scripts,
                asynchronous=self.asynchronous, scene=scene)

            if status.finished:
                self._do_finish(status, scene)
            else:
                status.onFinish = lambda finished: self._do_finish(finished, scene)

        except Exception as exc:
            self._do_fail(exc)

        return self.future

    @asynchronous
    async def unload(self) -> bool:
        if self.future is None:
            return False

        await self.future
        bge.logic.LibFree(self.path)
        self.status = None
        self.future = None
        return True

    @property
    def loading(self):
        return bool(self.status) and not self.status.finished

    @property
    def loaded(self):
        return bool(self.status) and bool(self.status.finished)

    def _do_finish(self, status, scene):
        self.scene = scene
        library = setdefault_library(scene, self.path)
        self.future.set_result(library)

    def _do_fail(self, exc):
        self.status = None
        self.future.set_exception(exc)
        self.future = None
        raise exc
