import aud

from bge.logic import expandPath


class Sound:

    Device = aud.device()
    Device.volume = .5

    def __init__(self, path: str, *, buffered: bool = False, volume: float = 1.0):
        self.factory = self._setup(aud.Factory(expandPath(path)))
        self.volume = volume
        if buffered:
            self.factory = self.factory.buffer()

    def _setup(self, factory):
        return factory

    def play(self):
        handle = self.Device.play(self.factory)
        handle.volume = self.volume
        return handle
