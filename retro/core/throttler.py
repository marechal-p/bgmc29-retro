import time


class Throttler:

    def __init__(self, delay: float, callback):
        self._deadline = 0
        self.callback = callback
        self.delay = delay

    def __call__(self, fallback=None):
        now = time.time()
        if self._deadline < time.time():
            self._deadline = now + self.delay
            return self.callback()
        return fallback
