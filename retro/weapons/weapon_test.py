import random

from retro.singletons.message_manager import MessageManager

# from retro.core.throttler import Throttler
from retro.core.weapon import Weapon
from retro.core.entity import Entity

from retro.network.protocol import write_player_attack
from retro.game_sounds import FiringSound, AttackHitSound


class WeaponTest(Weapon):

    def __init__(self, player_script):
        super().__init__()
        self.player_script = player_script

    def fire(self):
        camera = self.player_script.camera

        direction = camera.getAxisVect((0, 0, -1))
        target = camera.worldPosition + 1024 * direction
        origin = camera.worldPosition + .1 * direction

        FiringSound.play()
        hit, impact, normal = camera.rayCast(target, origin, target.magnitude, Entity.GAME_PROPERTY)
        if hit:
            self._hit_sound(impact)

        hit_id = hit[Entity.GAME_PROPERTY] if hit else 0x_FF_FF_FF_FF
        self._send_message(hit_id, 0x_FF_FF_FF_FF, origin, impact or target)

    def _hit_sound(self, position):
        sound = AttackHitSound.play()
        sound.pitch = random.random() + .5
        sound.location = position
        sound.relative = False

    def _send_message(self, hit, weapon, origin, impact):
        MessageManager.queue(
            write_player_attack(hit, weapon, origin, impact))
