from .input import InputType

__all__ = [
    'Axis',
    'Button',
]


class Axis(InputType):

    @classmethod
    def _union(cls, values):
        return max(values, key=abs)

    @classmethod
    def _intersection(cls, values):
        return sum(values) / len(values)

    def type(self):
        return Axis

    def threshold(self, threshold, fallback=0) -> float:
        value = self.value()
        if abs(value) > threshold:
            return value
        return fallback


class Button(InputType):

    RELEASED = 0
    RELEASING = 1
    PRESSING = 2
    PRESSED = 3

    STATES = (RELEASED, RELEASING, PRESSING, PRESSED)

    @classmethod
    def _union(cls, values):
        return max(values)

    @classmethod
    def _intersection(cls, values):
        return min(values)

    def type(self):
        return Button

    def released(self) -> bool:
        return self.value() == Button.RELEASED

    def releasing(self) -> bool:
        return self.value() == Button.RELEASING

    def pressing(self) -> bool:
        return self.value() == Button.PRESSING

    def pressed(self) -> bool:
        return self.value() == Button.PRESSED

    def active(self) -> bool:
        value = self.value()
        return value == Button.PRESSED or value == Button.PRESSING

    def inactive(self) -> bool:
        value = self.value()
        return value == Button.RELEASED or value == Button.RELEASING
