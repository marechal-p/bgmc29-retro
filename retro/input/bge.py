from bge import logic, events, render

import re

from .types import Axis, Button
from .input import Input

__all__ = [
    'bge_inputs'
]

JOYSTICK_MAX_COUNT = 8
JOYSTICK_MAX_BUTTONS = 32
JOYSTICK_MAX_AXIS = 8

BGE_STATE_MAP = {
    logic.KX_INPUT_NONE: Button.RELEASED,
    logic.KX_INPUT_JUST_RELEASED: Button.RELEASING,
    logic.KX_INPUT_JUST_ACTIVATED: Button.PRESSING,
    logic.KX_INPUT_ACTIVE: Button.PRESSED,
}


class KX_ButtonInput(Input):

    def __init__(self, keycode):
        self.keycode = keycode

    def _get_state(self):
        '''Returns the state of the key'''

    def type(self):
        return Button

    def value(self) -> int:
        return BGE_STATE_MAP[self._get_state()]


class Keyboard(KX_ButtonInput):
    def _get_state(self):
        return logic.keyboard.events[self.keycode]


class Mouse(KX_ButtonInput):

    @classmethod
    def center(self):
        logic.mouse.position = .5, .5

    def _get_state(self):
        return logic.mouse.events[self.keycode]


class Joystick:

    def __init__(self, index: int):
        self.index = index

    def get(self):
        index = self.index
        joysticks = logic.joysticks
        if index >= len(joysticks):
            return None
        return joysticks[index]


class JoystickButton(Input, Joystick):

    def __init__(self, joystick: int, button: int):
        Joystick.__init__(self, joystick)
        self.button = button

    def type(self):
        return Button

    def value(self):
        button = self.button
        joystick = self.get()
        if joystick is None:
            return None
        if button in joystick.activeButtons:
            return Button.PRESSED
        return Button.RELEASED


class JoystickAxis(Input, Joystick):

    def __init__(self, joystick: int, axis: int):
        Joystick.__init__(self, joystick)
        self.axis = axis

    def type(self):
        return Axis

    def value(self):
        axis = self.axis
        joystick = self.get()
        if (joystick is None) or (axis >= joystick.numAxis):
            return None
        return joystick.axisValues[axis]


class MouseDelta(Input):

    def __init__(self, axis: int):
        self.axis: int = axis
        self.bound: float = 128.

    def type(self):
        return Axis

    def get(self) -> float:
        return logic.mouse.position[self.axis]

    def value(self) -> float:
        current = self.get()
        bound = self.bound

        w, h = window_size = render.getWindowWidth() - 1, render.getWindowHeight() - 1
        center = (int(w * .5) / w, int(h * .5) / h)[self.axis]

        delta = (center - current) * window_size[self.axis]
        self.last = current
        return max(min(delta / bound, bound), -bound)


def _register_inputs() -> dict:
    registry = {}

    pattern = re.compile(r'key$')

    # mouse/keyboard buttons
    for name in dir(events):
        keycode = getattr(events, name)

        if not isinstance(keycode, int):
            continue  # not a keycode

        key = pattern.sub('', name.lower())

        if keycode in logic.keyboard.events:
            input = Keyboard(keycode)
        elif keycode in logic.mouse.events:
            input = Mouse(keycode)
        else:
            continue

        registry[key] = input

    # mouse axis
    registry['mouse.dx'] = MouseDelta(0)
    registry['mouse.dy'] = MouseDelta(1)

    # joysticks
    for joystickIndex in range(JOYSTICK_MAX_COUNT):
        joystickName = f'joystick{joystickIndex}'

        # joysticks buttons
        for buttonIndex in range(JOYSTICK_MAX_BUTTONS):
            inputName = f'{joystickName}.button{buttonIndex}'
            registry[inputName] = JoystickButton(joystickIndex, buttonIndex)

        # joysticks axis
        for axisIndex in range(JOYSTICK_MAX_AXIS):
            inputName = f'{joystickName}.axis{axisIndex}'
            registry[inputName] = JoystickAxis(joystickIndex, axisIndex)

    return registry


bge_inputs = _register_inputs()
