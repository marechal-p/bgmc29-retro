from typing import Tuple, List, Dict, Callable, Any

import re

from itertools import chain

from .input import Input, InputType

__all__ = [
    'Keymap',
]

WHITESPACE_RE = re.compile(r'\s')


class Action(Input):

    def __init__(self, keymap: 'Keymap', input_type: InputType, defaults=[]):
        self._bindings: Dict[str, Callable[[], Any]] = {}
        self._links: List[Callable[[], Any]] = []
        self._keymap = keymap
        self._type = input_type
        self.input = input_type(self)
        self.bind(*defaults)

    def _parse(self, input_string: str) -> Tuple[str, List[str]]:
        parts = WHITESPACE_RE.sub('', input_string).lower().split('+')
        return '+'.join(sorted(parts)), parts

    def _resolve_input(self, input_string: str):
        input = self._keymap._resolve_input(input_string)
        input_type = input.type()

        if input_type is self._type:
            return input

        raise TypeError(f'expected {self._type}, got {input_type} from "{input_string}"')

    def _filter(self, values):
        return (value for value in values if value is not None)

    def _create_intersection(self, inputs):
        return (lambda intersection, values:
            lambda: intersection(tuple(self._filter(
                value() for value in values)))
        )(self._type._intersection, tuple(input.value for input in inputs))

    @property
    def bindings(self):
        return tuple(self._bindings.keys())

    def clear(self):
        self._bindings.clear()

    def bind(self, *input_strings: str) -> None:
        for input_string in input_strings:
            normalized, parts = self._parse(input_string)

            if normalized in self._bindings:
                continue

            inputs = [*map(self._resolve_input, parts)]
            self._bindings[normalized] = self._create_intersection(inputs)

    def link(self, input: Input, converter: Callable[[Any], Any]) -> None:
        self._links.append(lambda: converter(input))

    def type(self):
        return self._type

    def value(self):
        return self._type._union(self._filter(value() for value in
            chain(self._links, self._bindings.values())))


class Keymap:

    def __init__(self, input_registry: Dict[str, Input]):
        self._input_registry = input_registry
        self._defaults: Dict[str, List[str]] = {}
        self._actions: Dict[str, Action] = {}

    def _resolve_input(self, input_string: str) -> Input:
        return self._input_registry[input_string]

    def define(self, action_name: str, input_type: InputType, defaults=[]) -> Input:
        action = self._actions.get(action_name, None)
        if action is not None:
            raise ValueError(f'{action_name} is already defined')
        self._actions[action_name] = action = Action(self, input_type, defaults)
        self._defaults[action_name] = defaults.copy()
        return action.input

    def link(self, target_action: str, sub_action: str, converter: Callable) -> None:
        self.get(target_action).link(self.get(sub_action).input, converter)

    def get(self, action_name: str) -> Action:
        return self._actions[action_name]

    def to_dict(self) -> Dict[str, List[str]]:
        defaulted = self._defaults.copy()
        defaulted.update({
            action_name: action.bindings
            for action_name, action in self._actions.items()
        })
        return defaulted

    def from_dict(self, data: Dict[str, Any]) -> None:
        defaulted: Dict[str, List[str]] = self._defaults.copy()
        defaulted.update(data)

        for action_name, bindings in defaulted.items():
            if action_name in self._actions:
                action = self.get(action_name)
                action.clear()
                action.bind(*bindings)

    def save(self, configuration: dict) -> None:
        configuration.update(self.to_dict())

    def load(self, configuration: dict) -> None:
        self.from_dict(configuration)
        self.save(configuration)
