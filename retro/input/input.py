from typing import Generic, TypeVar, Optional, Type

import abc

__all__ = [
    'Input',
    'InputType',
]

T = TypeVar('T')


class Input(abc.ABC, Generic[T]):

    @abc.abstractmethod
    def type(self) -> 'Type[InputType]':
        '''
        Returns the input type of this input.
        '''

    @abc.abstractmethod
    def value(self) -> Optional[T]:
        '''
        Returns the current value of the input.
        '''


class InputType(Input[T]):

    @abc.abstractclassmethod
    def _union(cls, values):
        '''
        Returns (A or B or C or ...)
        '''

    @abc.abstractclassmethod
    def _intersection(cls, values):
        '''
        Returns (A and B and C and ...)
        '''

    def __init__(self, input: Input[T]):
        self._input = input

    def value(self) -> Optional[T]:
        return self._input.value()
