from retro.core.libload import Blend


def _path(*parts):
    from os.path import join, realpath
    return realpath(join(*parts))


player = Blend(_path('blends', 'player.blend'))
